#!/bin/bash
R CMD Rserve --no-save \
  --RS-workdir /srv/metrics.torproject.org/metrics/website/rserve/workdir \
  --RS-source /srv/metrics.torproject.org/metrics/src/main/R/rserver/rserve-init.R >> rserve.log 2>&1
