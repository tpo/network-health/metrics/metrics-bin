#!/bin/bash
METRICS_PASS=$(</srv/metrics.torproject.org/metrics/.metrics_db_pw)

java -XX:+UnlockExperimentalVMOptions -XX:+UseShenandoahGC -Xms256m -Xmx12g -DLOGBASE=/srv/metrics.torproject.org/metrics/work/modules/logs/ -Dmetrics.basedir=/srv/metrics.torproject.org/metrics -Dmetrics.dbuser=metrics -Dmetrics.dbpass="${METRICS_PASS}" -jar /srv/metrics.torproject.org/metrics/metrics-web-1.4.5.jar

status=$?

exit $status;
